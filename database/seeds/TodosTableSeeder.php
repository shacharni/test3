<?php

use Illuminate\Database\Seeder;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todos')->insert([
            [
        'title'=>'todo1',
        'user_id'=>'1',
        
    ],
    [
        'title'=>'todo2',
        'user_id'=>'2',
   ],
   [
    'title'=>'todo3',
        'user_id'=>'3',
    ],
 
]);  
    }
}
