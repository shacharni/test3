@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<h1> this is a to do list</h1>
    <head>
    </head>
    <body>
    @if (Request::is('todos')) 
<a href="{{action('TodoController@alltodos')}}">all todos</a>
@else
<a href="{{action('TodoController@index')}}">my todos</a>
@endif

    <ul>
   @foreach($todos as $todo)
   @if ($todo->status)

   <label style="color: green; font-weight: bold;"><li >{{$todo->title}}</li>
   <input type = 'checkbox' name="termsChkbx" id ="{{$todo->id}}" checked onclick="updateColor(this)"/>
   </label>
       @else
       <label style="color: red; font-weight: bold;"><li >{{$todo->title}}</li>
           <input type = 'checkbox'  name="termsChkbx" id ="{{$todo->id}}" onclick="updateColor(this)"/>
           </label>
          
       @endif 
         
       @can('manager') 
       
       <a href="{{route('todos.edit',$todo->id)}}">edit</a> 
       <a href="{{route('todos.duplication', ['title' =>$todo->title])}}">
       duplication</a> <form method='post' action="{{action('TodoController@destroy',$todo->id)}}"> 
    @csrf
    @method('DELETE')

    <div class = "form-group">
        <input type="submit" class="btn btn-link" name="submit" value="delete">
    </div>
</form>@endcan
          @endforeach
          </ul>
          @can('manager') <a href="{{route('todos.create')}}">Create a new todo</a>@endcan
          <script>
      $(document).ready(function(){
          $(":checkbox").click(function(event){
              console.log(event.target.id)
               $.ajax({
                   url: "{{url('todos')}}"+'/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType:'application/json',
                   data:JSON.stringify({'status':event.target.checked, _token:"{{csrf_token()}}"}),
                   processData:false,
                  success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
       function updateColor(el){
  el.parentNode.style.color = el.checked ? "green" : "#FF0000"
}
   </script>  

   </body>
</html>
@endsection