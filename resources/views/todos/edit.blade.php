@extends('layouts.app')

@section('content')
<h1>Update a todo</h1>
<form method='post' action="{{action('TodoController@update',$todo->id)}}">
    @csrf
    @method('PATCH')

    <div class="form-group">
    <label for ="edit"> todo to update</label>
    <div>
        <label for ="title"> new title</label>
        <input type="text" class= "form-control" name="title" value="{{$todo->title}}">
        </div>
    </div>
    <div class = "form-group">
        <input type="submit" class="form-controll" name="submit" value="update">
    </div>
</form>
@endsection